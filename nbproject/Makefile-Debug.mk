#
# Generated Makefile - do not edit!
#
# Edit the Makefile in the project folder instead (../Makefile). Each target
# has a -pre and a -post target defined where you can add customized code.
#
# This makefile implements configuration specific macros and targets.


# Environment
MKDIR=mkdir
CP=cp
GREP=grep
NM=nm
CCADMIN=CCadmin
RANLIB=ranlib
CC=gcc
CCC=g++
CXX=g++
FC=gfortran
AS=as

# Macros
CND_PLATFORM=GNU-Linux
CND_DLIB_EXT=so
CND_CONF=Debug
CND_DISTDIR=dist
CND_BUILDDIR=build

# Include project Makefile
include Makefile

# Object Directory
OBJECTDIR=${CND_BUILDDIR}/${CND_CONF}/${CND_PLATFORM}

# Object Files
OBJECTFILES= \
	${OBJECTDIR}/src/CLFUSCache.o \
	${OBJECTDIR}/src/LRUCache.o \
	${OBJECTDIR}/src/MemoryCache.o \
	${OBJECTDIR}/src/main.o

# Test Directory
TESTDIR=${CND_BUILDDIR}/${CND_CONF}/${CND_PLATFORM}/tests

# Test Files
TESTFILES= \
	${TESTDIR}/TestFiles/f1

# Test Object Files
TESTOBJECTFILES= \
	${TESTDIR}/tests/MemoryCacheTest.o

# C Compiler Flags
CFLAGS=

# CC Compiler Flags
CCFLAGS=
CXXFLAGS=

# Fortran Compiler Flags
FFLAGS=

# Assembler Flags
ASFLAGS=

# Link Libraries and Options
LDLIBSOPTIONS=../third-party/lib/Poco/libPocoData.a ../third-party/lib/Poco/libPocoDataSQLite.a ../third-party/lib/Poco/libPocoNet.a ../third-party/lib/Poco/libPocoNetSSL.a ../third-party/lib/Poco/libPocoXML.a ../third-party/lib/Poco/libPocoZip.a ../third-party/lib/Poco/libPocoJSON.a ../third-party/lib/Poco/libPocoCrypto.a ../third-party/lib/Poco/libPocoUtil.a ../third-party/lib/Poco/libPocoFoundation.a

# Build Targets
.build-conf: ${BUILD_SUBPROJECTS}
	"${MAKE}"  -f nbproject/Makefile-${CND_CONF}.mk ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/memory-cache

${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/memory-cache: ../third-party/lib/Poco/libPocoData.a

${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/memory-cache: ../third-party/lib/Poco/libPocoDataSQLite.a

${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/memory-cache: ../third-party/lib/Poco/libPocoNet.a

${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/memory-cache: ../third-party/lib/Poco/libPocoNetSSL.a

${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/memory-cache: ../third-party/lib/Poco/libPocoXML.a

${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/memory-cache: ../third-party/lib/Poco/libPocoZip.a

${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/memory-cache: ../third-party/lib/Poco/libPocoJSON.a

${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/memory-cache: ../third-party/lib/Poco/libPocoCrypto.a

${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/memory-cache: ../third-party/lib/Poco/libPocoUtil.a

${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/memory-cache: ../third-party/lib/Poco/libPocoFoundation.a

${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/memory-cache: ${OBJECTFILES}
	${MKDIR} -p ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}
	${LINK.cc} -o ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/memory-cache ${OBJECTFILES} ${LDLIBSOPTIONS}

${OBJECTDIR}/src/CLFUSCache.o: src/CLFUSCache.cpp
	${MKDIR} -p ${OBJECTDIR}/src
	${RM} "$@.d"
	$(COMPILE.cc) -g -Wall -Iinc -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/src/CLFUSCache.o src/CLFUSCache.cpp

${OBJECTDIR}/src/LRUCache.o: src/LRUCache.cpp
	${MKDIR} -p ${OBJECTDIR}/src
	${RM} "$@.d"
	$(COMPILE.cc) -g -Wall -Iinc -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/src/LRUCache.o src/LRUCache.cpp

${OBJECTDIR}/src/MemoryCache.o: src/MemoryCache.cpp
	${MKDIR} -p ${OBJECTDIR}/src
	${RM} "$@.d"
	$(COMPILE.cc) -g -Wall -Iinc -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/src/MemoryCache.o src/MemoryCache.cpp

${OBJECTDIR}/src/main.o: src/main.cpp
	${MKDIR} -p ${OBJECTDIR}/src
	${RM} "$@.d"
	$(COMPILE.cc) -g -Wall -Iinc -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/src/main.o src/main.cpp

# Subprojects
.build-subprojects:

# Build Test Targets
.build-tests-conf: .build-tests-subprojects .build-conf ${TESTFILES}
.build-tests-subprojects:

${TESTDIR}/TestFiles/f1: ${TESTDIR}/tests/MemoryCacheTest.o ${OBJECTFILES:%.o=%_nomain.o}
	${MKDIR} -p ${TESTDIR}/TestFiles
	${LINK.cc} -o ${TESTDIR}/TestFiles/f1 $^ ${LDLIBSOPTIONS}   -lgtest -lpthread 


${TESTDIR}/tests/MemoryCacheTest.o: tests/MemoryCacheTest.cpp 
	${MKDIR} -p ${TESTDIR}/tests
	${RM} "$@.d"
	$(COMPILE.cc) -g -Wall -Iinc -I. -std=c++11 -MMD -MP -MF "$@.d" -o ${TESTDIR}/tests/MemoryCacheTest.o tests/MemoryCacheTest.cpp


${OBJECTDIR}/src/CLFUSCache_nomain.o: ${OBJECTDIR}/src/CLFUSCache.o src/CLFUSCache.cpp 
	${MKDIR} -p ${OBJECTDIR}/src
	@NMOUTPUT=`${NM} ${OBJECTDIR}/src/CLFUSCache.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} "$@.d";\
	    $(COMPILE.cc) -g -Wall -Iinc -std=c++11 -Dmain=__nomain -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/src/CLFUSCache_nomain.o src/CLFUSCache.cpp;\
	else  \
	    ${CP} ${OBJECTDIR}/src/CLFUSCache.o ${OBJECTDIR}/src/CLFUSCache_nomain.o;\
	fi

${OBJECTDIR}/src/LRUCache_nomain.o: ${OBJECTDIR}/src/LRUCache.o src/LRUCache.cpp 
	${MKDIR} -p ${OBJECTDIR}/src
	@NMOUTPUT=`${NM} ${OBJECTDIR}/src/LRUCache.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} "$@.d";\
	    $(COMPILE.cc) -g -Wall -Iinc -std=c++11 -Dmain=__nomain -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/src/LRUCache_nomain.o src/LRUCache.cpp;\
	else  \
	    ${CP} ${OBJECTDIR}/src/LRUCache.o ${OBJECTDIR}/src/LRUCache_nomain.o;\
	fi

${OBJECTDIR}/src/MemoryCache_nomain.o: ${OBJECTDIR}/src/MemoryCache.o src/MemoryCache.cpp 
	${MKDIR} -p ${OBJECTDIR}/src
	@NMOUTPUT=`${NM} ${OBJECTDIR}/src/MemoryCache.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} "$@.d";\
	    $(COMPILE.cc) -g -Wall -Iinc -std=c++11 -Dmain=__nomain -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/src/MemoryCache_nomain.o src/MemoryCache.cpp;\
	else  \
	    ${CP} ${OBJECTDIR}/src/MemoryCache.o ${OBJECTDIR}/src/MemoryCache_nomain.o;\
	fi

${OBJECTDIR}/src/main_nomain.o: ${OBJECTDIR}/src/main.o src/main.cpp 
	${MKDIR} -p ${OBJECTDIR}/src
	@NMOUTPUT=`${NM} ${OBJECTDIR}/src/main.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} "$@.d";\
	    $(COMPILE.cc) -g -Wall -Iinc -std=c++11 -Dmain=__nomain -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/src/main_nomain.o src/main.cpp;\
	else  \
	    ${CP} ${OBJECTDIR}/src/main.o ${OBJECTDIR}/src/main_nomain.o;\
	fi

# Run Test Targets
.test-conf:
	@if [ "${TEST}" = "" ]; \
	then  \
	    ${TESTDIR}/TestFiles/f1 || true; \
	else  \
	    ./${TEST} || true; \
	fi

# Clean Targets
.clean-conf: ${CLEAN_SUBPROJECTS}
	${RM} -r ${CND_BUILDDIR}/${CND_CONF}

# Subprojects
.clean-subprojects:

# Enable dependency checking
.dep.inc: .depcheck-impl

include .dep.inc
