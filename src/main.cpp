/* 
 * File:   main.cpp
 * Author: hungcx
 *
 * Created on October 12, 2017, 9:32 AM
 */

#include <iostream> 
#include <memory.h>

#include "memorycache/MemoryCache.h"
#include "memorycache/LRUCache.h"
#include "memorycache/CLFUSCache.h"

int main(int argc, char** argv) {
    std::unique_ptr<memorycache::MemoryCache> cache(new memorycache::CLFUSCache(100));
    
    std::string key_1 = "one";
    std::string key_2 = "two";
    
    std::unique_ptr<char[]> data_1(new char[1]);
    memset(data_1.get(), '1', 1);
    memorycache::MemoryCache::Info info_1;
    info_1.size = 1; info_1.time = time(NULL);
    
    std::unique_ptr<char[]> data_2(new char[2]);
    memset(data_2.get(), '2', 2);
    memorycache::MemoryCache::Info info_2;
    info_2.size = 2; info_2.time = time(NULL);
    
    cache->set(key_1, data_1, info_1);
    cache->set(key_2, data_2, info_2);
    
    char* result_1 = cache->get(key_1);
    std::cout << result_1[0] << "\n";
    
    char* result_2 = cache->get(key_2);
    std::cout << result_2[1] << "\n";
    
    return 0;
}

