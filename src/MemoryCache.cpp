/* 
 * File:   MemoryCache.cpp
 * Author: hungcx
 * 
 * Created on July 31, 2017, 4:22 PM
 */

#include "memorycache/MemoryCache.h"

namespace memorycache {

MemoryCache::MemoryCache(const size_t capacity):
capacity_(capacity) {
}

MemoryCache::~MemoryCache() {}

size_t MemoryCache::getCurrentSize() {
	return size_;
}

} // namespace memorycache

