/* 
 * File:   CLFUSCache.cpp
 * Author: hungcx
 * 
 * Created on August 10, 2017, 9:33 AM
 */

#include <iostream>

#include "memorycache/CLFUSCache.h"

namespace memorycache {

CLFUSCache::CLFUSCache(const size_t capacity) :
MemoryCache::MemoryCache(capacity) {
}

CLFUSCache::~CLFUSCache() {
}

bool CLFUSCache::contain(const std::string& key) {
	std::lock_guard<std::mutex> lock(mutex_);
	Cache::iterator it = cache_.find(key);
	return (it != cache_.end());
}

char* CLFUSCache::get(const std::string& key) {
	std::lock_guard<std::mutex> lock(mutex_);

	Cache::iterator it = cache_.find(key);

	// No value found
	if (it == cache_.end())
		return nullptr;

	Entry& entry = it->second;
	char* result = entry.data.get();
	entry.hit();
	requeueCache(key, entry);
	return result;
}

Poco::Optional<MemoryCache::Info> CLFUSCache::getInfo(const std::string& key) {
	std::lock_guard<std::mutex> lock(mutex_);

	Cache::iterator it = cache_.find(key);
	// No value found
	if (it == cache_.end())
		return Poco::Optional<MemoryCache::Info>();
	Entry& entry = it->second;
	return entry.info;
}

bool CLFUSCache::set(const std::string& key, std::unique_ptr<char[]>& data,
		const MemoryCache::Info& info) {
	std::lock_guard<std::mutex> lock(mutex_);
    
	if (cache_.count(key))
		removeFromCache(key);

	if (history_.count(key) == 0)
		addNew(key, info);

	// Take candidate from history and increase hits
	Entry& candidate = history_[key];
	candidate.hit();
    
	// Main loop to find if candidate will be cached
	while (size_ + info.size > capacity_ && !cachelist_.empty()) {
		// LRU object is the victim
		std::string victim = cachelist_.back();
		Entry& victim_entry = cache_[victim];

		// Candidate is considered promoting
		if (cacheValue(candidate) >= cacheValue(victim_entry)) {
			removeFromCache(victim);
			if (size_ + info.size <= capacity_)
				break;

		// Candidate is not cached, candidate and victim are requeued
		} else {
			requeueHistory(key, candidate);

			resetHit(victim_entry);
			requeueCache(victim, victim_entry);

			return false;
		}
		
		// One clock period
		if (key != historylist_.back())
			tick();
	}

	// Remove entry from history
	Entry entry = std::move(history_[key]);
	history_.erase(key);
	historylist_.erase(entry.iter);

	// Add to cache
	cachelist_.push_front(key);
	entry.iter = cachelist_.begin();
	entry.data = std::move(data);
	entry.info = info;
	cache_[key] = std::move(entry);
	size_ += info.size;
    
    return true;
}

void CLFUSCache::remove(const std::string& key) {
	std::lock_guard<std::mutex> lock(mutex_);

	if (cache_.count(key)) 
        removeFromCache(key);
}

double CLFUSCache::cacheValue(Entry& entry) {
	return (double) entry.hits / (entry.info.size + OVERHEAD);
}

void CLFUSCache::resetHit(Entry& entry) {
	entry.hits = 0;
}

void CLFUSCache::tick() {
	if (historylist_.empty()) return;
	std::string victim = historylist_.back();
	Entry& victim_entry = history_[victim];
	
	if (victim_entry.hits > 1) {
		resetHit(victim_entry);
		requeueHistory(victim, victim_entry);
	} else {
		history_.erase(victim);
		historylist_.pop_back();
	}
}

void CLFUSCache::requeueCache(const std::string& key, Entry& entry) {
	cachelist_.erase(entry.iter);
	cachelist_.push_front(key);
	entry.iter = cachelist_.begin();
}

void CLFUSCache::requeueHistory(const std::string& key, Entry& entry) {
	historylist_.erase(entry.iter);
	historylist_.push_front(key);
	entry.iter = historylist_.begin();
}

void CLFUSCache::addNew(const std::string& key, const Info& info) {
	historylist_.push_front(key);

	Entry new_entry;
	new_entry.iter = historylist_.begin();
	new_entry.hits = 0;
	new_entry.info = info;

	history_[key] = std::move(new_entry);
}

void CLFUSCache::removeFromCache(const std::string& key) {
	// Remove from cache
	Entry entry = std::move(cache_[key]);
	cachelist_.erase(entry.iter);
	cache_.erase(key);
	entry.data.reset(nullptr);
	size_ -= entry.info.size;

	// Insert to history
	historylist_.push_front(key);
	entry.iter = historylist_.begin();
	history_[key] = std::move(entry);
}

} // namespace memorycache
