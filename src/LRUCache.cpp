/* 
 * File:   LRUCache.cpp
 * Author: hungcx
 * 
 * Created on August 1, 2017, 12:01 PM
 */

#include "memorycache/LRUCache.h"

namespace memorycache {

LRUCache::LRUCache(const size_t capacity) :
MemoryCache::MemoryCache(capacity) {
}

LRUCache::~LRUCache() {
}

bool LRUCache::contain(const std::string& key) {
	std::lock_guard<std::mutex> lock(mutex_);
	Cache::iterator it = cache_.find(key);
	return (it != cache_.end());
}

char* LRUCache::get(const std::string& key) {
	std::lock_guard<std::mutex> lock(mutex_);
	Cache::iterator it = cache_.find(key);
	
	// No value found
	if (it == cache_.end())
		return nullptr;
	
	Entry& entry = it->second;
	char* result = entry.data.get();
	update(key, entry);
	return result;
}

Poco::Optional<MemoryCache::Info> LRUCache::getInfo(const std::string& key) {
	std::lock_guard<std::mutex> lock(mutex_);

	Cache::iterator it = cache_.find(key);
	// No value found
	if (it == cache_.end())
		return Poco::Optional<MemoryCache::Info>();
	Entry& entry = it->second;
	return entry.info;
}

bool LRUCache::set(const std::string& key, std::unique_ptr<char[]>& data,
		const MemoryCache::Info& info) {
	std::lock_guard<std::mutex> lock(mutex_);
	Cache::iterator it = cache_.find(key);
	if (it != cache_.end()) {
		size_ -= it->second.info.size;
		list_.erase(it->second.iter);
		cache_.erase(it);
	}
		
	while (!cache_.empty() && size_ + info.size > capacity_)
		remove();
	if (size_ + info.size > capacity_) return false;
    
	add(key, data, info);
    return true;
}

void LRUCache::remove(const std::string& key) {
	std::lock_guard<std::mutex> lock(mutex_);
	Cache::iterator it = cache_.find(key);
	if (it != cache_.end()) {
		size_ -= it->second.info.size;
		list_.erase(it->second.iter);
		cache_.erase(it);
	}
}

void LRUCache::add(const std::string& key, std::unique_ptr<char[]>& data,
		const Info& info) {
	size_ += info.size;
	list_.push_front(key);
	
	Entry new_entry;
	new_entry.data = std::move(data);
	new_entry.iter = list_.begin();
	new_entry.info = info;
	
	cache_[key] = std::move(new_entry);
}

void LRUCache::remove() {
	std::string& key = list_.back();
	list_.pop_back();
	
	Cache::iterator it = cache_.find(key);
	size_ -= it->second.info.size;
	cache_.erase(it);
}

void LRUCache::update(const std::string& key, Entry& entry) {
	LRUList::iterator pos = entry.iter;
	list_.erase(pos);
	list_.push_front(key);
	entry.iter = list_.begin();
}

} // namespace memorycache

