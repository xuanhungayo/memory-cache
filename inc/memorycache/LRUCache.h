/* 
 * File:   LRUCache.h
 * Author: hungcx
 *
 * Created on August 1, 2017, 12:01 PM
 */

#ifndef LRUCACHE_H
#define LRUCACHE_H

#include <list>
#include <utility>
#include <unordered_map>
#include <map>
#include <memory>

#include "memorycache/MemoryCache.h"

namespace memorycache {

// Memory cache that uses Least Recently Used strategy
class LRUCache : public MemoryCache {
public:
	typedef std::list<std::string> LRUList;

	struct Entry {
		std::unique_ptr<char[] > data;
		LRUList::iterator iter;
		Info info;
	};
	typedef std::unordered_map<std::string, Entry> Cache;

	LRUCache(const size_t capacity);
	virtual ~LRUCache();

	bool contain(const std::string& key) override;
	char* get(const std::string& key) override;
	Poco::Optional<Info> getInfo(const std::string& key) override;
	bool set(const std::string& key, std::unique_ptr<char[]>& data, 
			const Info& info) override;
	void remove(const std::string& key) override;

private:
	Cache cache_;
	LRUList list_;

	// Add key-value to the cache in case no key exists
	void add(const std::string& key, std::unique_ptr<char[]>& data, 
			const Info& info);
	
	// Evict the least recently used key-value
	void remove();
	
	// Update the value for key that already exists
	// Move the key to the most frequently used
	void update(const std::string& key, Entry& entry);
};

} // namespace memorycache

#endif /* LRUCACHE_H */