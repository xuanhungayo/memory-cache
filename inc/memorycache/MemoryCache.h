/* 
 * File:   MemoryCache.h
 * Author: hungcx
 *
 * Created on July 31, 2017, 4:22 PM
 */

#ifndef MEMORYCACHE_H
#define MEMORYCACHE_H

#include <string>
#include <memory>
#include <mutex>

#include "Poco/Optional.h"

namespace memorycache {

// Abstract class of memory cache
class MemoryCache {
public:
	struct Info {	
		size_t size;
		time_t time;
	};
	
	MemoryCache(const size_t capacity);
	virtual ~MemoryCache();
	
	// Return true if object is in cache
	virtual bool contain(const std::string& key) = 0;
	
	// Return pointer to the cached object
	// If no value exists, return nullptr
	virtual char* get(const std::string& key) = 0;
	
	// Return information of a cached object
	// If key is not found, return null Poco::Optional value
	virtual Poco::Optional<Info> getInfo(const std::string& key) = 0;
	
	// Set key value to the cache,
	// Return true if object was set successfully, false otherwise
	virtual bool set(const std::string& key, std::unique_ptr<char[]>& data, 
			const Info& info) = 0;
	
	// Remove key value from the cache
	// If key is not found, it will be ignored
	virtual void remove(const std::string& key) = 0;
	
	size_t getCurrentSize();
	
protected:
	// Mutex variable to ensure thread-safe
	std::mutex mutex_;
	
	// Capacity in byte
	size_t capacity_;
	
	// Current size in byte
	size_t size_ = 0;
};

} // namespace memorycache

#endif /* MEMORYCACHE_H */

