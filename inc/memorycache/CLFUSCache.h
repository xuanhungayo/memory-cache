/* 
 * File:   CLFUSCache.h
 * Author: hungcx
 *
 * Created on August 10, 2017, 9:33 AM
 */

#ifndef CLFUSCACHE_H
#define CLFUSCACHE_H

#include <list>
#include <unordered_map>

#include "memorycache/MemoryCache.h"

namespace memorycache {

// Memory cache that uses Clock Least Frequently Used by Size strategy, source from
// https://docs.trafficserver.apache.org/en/latest/developer-guide/cache-architecture/ram-cache.en.html#new-ram-cache-algorithm-clfus
class CLFUSCache : public MemoryCache {
public:
	// Overhead value to determine cache value of each object
	const int OVERHEAD = 0;

	typedef std::list<std::string> LRUList;

	// Cache entry
	struct Entry {
		std::unique_ptr<char[] > data;
		LRUList::iterator iter;
		uint32_t hits;
		Info info;

		void hit() {
			hits++;
		}
	};
	
	typedef std::unordered_map<std::string, Entry> Cache;

	CLFUSCache(const size_t capacity);
	virtual ~CLFUSCache();

	bool contain(const std::string& key) override;
	char* get(const std::string& key) override;
	Poco::Optional<Info> getInfo(const std::string& key) override;
	bool set(const std::string& key, std::unique_ptr<char[]>& data, 
			const Info& info) override;
	void remove(const std::string& key) override;

private:
	// Map that contains objects with information and data
	Cache cache_;

	// Map that contains recent objects with information only 
	Cache history_;

	// Lists only contain keys
	LRUList cachelist_;
	LRUList historylist_;

	double cacheValue(Entry& entry);
	void resetHit(Entry& entry);
	
	// One clock period
	void tick();
	
	void requeueCache(const std::string& key, Entry& entry);
	void requeueHistory(const std::string& key, Entry& entry);
	
	// And brand new object to history list with information
	void addNew(const std::string& key, const Info& info);
	
	// Remove and add to history list
	void removeFromCache(const std::string& key); 
};

} // namespace memorycache

#endif /* CLFUSCACHE_H */

