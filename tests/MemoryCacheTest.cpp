/* 
 * File:   LRUTest.cpp
 * Author: hungcx
 *
 * Created on October 12, 2017, 2:28 PM
 */
#include <memory.h>
#include <gtest/gtest.h>
#include <Poco/Optional.h>

#include "memorycache/LRUCache.h"
#include "memorycache/CLFUSCache.h"

const int CACHE_CAPACITY = 100;
const int DATA_NUM = 100;
const int VALUE_SIZE = 10;

class MemoryCacheTest : public testing::Test {
protected:

    void SetUp() {
        cache.reset(new memorycache::CLFUSCache(CACHE_CAPACITY));
        
        for (int i = 0; i < DATA_NUM; i++) {
            key[i] = std::to_string(i);
            data[i].reset(new char(VALUE_SIZE));
            memset(data[i].get(), i, VALUE_SIZE);
            info[i].size = VALUE_SIZE;
            info[i].time = time(NULL);
        }
    }

    std::unique_ptr<memorycache::MemoryCache> cache;
    std::string key[DATA_NUM];
    std::unique_ptr<char[] > data[DATA_NUM];
    memorycache::MemoryCache::Info info[DATA_NUM];
    char* result[DATA_NUM];
};

TEST_F(MemoryCacheTest, Contain) {
    cache->set(key[0], data[0], info[0]);
    cache->set(key[1], data[1], info[1]);
    ASSERT_TRUE(cache->contain("0"));
    ASSERT_TRUE(cache->contain("1"));
    cache->remove("0");
    ASSERT_FALSE(cache->contain("0"));
    ASSERT_TRUE(cache->contain("1"));
}

TEST_F(MemoryCacheTest, BasicGetSet) {
    cache->set(key[0], data[0], info[0]);
    cache->set(key[3], data[3], info[3]);
    cache->set(key[50], data[50], info[50]);

    result[0] = cache->get(key[0]);
    for (int i = 0; i < VALUE_SIZE; i++)
        ASSERT_EQ(result[0][i], 0);

    result[1] = cache->get(key[1]);
    ASSERT_EQ(result[1], nullptr);

    result[50] = cache->get(key[50]);
    for (int i = 0; i < VALUE_SIZE; i++)
        ASSERT_EQ(result[50][i], 50);
}

TEST_F(MemoryCacheTest, GetInfo) {
    cache->set(key[1], data[1], info[1]);
    cache->set(key[13], data[13], info[13]);

    Poco::Optional<memorycache::MemoryCache::Info> info_1 = cache->getInfo(key[1]);
    ASSERT_EQ(info_1.value().size, info[1].size);
    ASSERT_EQ(info_1.value().time, info[1].time);

    ASSERT_FALSE(cache->getInfo(key[2]).isSpecified());
}

TEST_F(MemoryCacheTest, Mixed) {
    for (int i = 0; i < 10; i++)
        cache->set(key[i], data[i], info[i]);
    cache->remove(key[2]);
    cache->remove(key[4]);
    ASSERT_TRUE(cache->contain(key[9]));
    ASSERT_FALSE(cache->contain(key[4]));

    result[3] = cache->get(key[3]);
    for (int i = 0; i < VALUE_SIZE; i++)
        ASSERT_EQ(result[3][i], 3);

    cache->set(key[2], data[2], info[2]);
    Poco::Optional<memorycache::MemoryCache::Info> info_2 = cache->getInfo(key[2]);
    ASSERT_EQ(info_2.value().size, info[2].size);
    ASSERT_EQ(info_2.value().time, info[2].time);
}

TEST_F(MemoryCacheTest, BigRandom) {
    for(int i = 0; i < 1e6; i++) {
        int key_num = rand() % 100;
        int type = rand() % 4;
        if (type == 0) {
            cache->set(key[key_num], data[key_num], info[key_num]);
        }
        if (type == 1) {
            cache->get(key[key_num]);
        }
        if (type == 2) {
            cache->contain(key[key_num]);
        }
        if (type == 3) {
            cache->remove(key[key_num]);
        }
    }
}

int main(int argc, char** argv) {
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
